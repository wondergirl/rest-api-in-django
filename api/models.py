from django.db import models

# Create your models here.

class Playlist(models.Model):
    name = models.CharField(max_length=200)

    class Meta:
        ordering = ('id',)


class Song(models.Model):
    soundCloudID = models.IntegerField(blank=False, unique=True)
    songTitle = models.CharField(max_length=200)
    isPlayed = models.BooleanField(default=False)
    playlist = models.ForeignKey(Playlist, blank=False, related_name="songs")
    isPlaying = models.BooleanField(default=False)
    karma = models.IntegerField(default=0)

    class Meta:
        ordering = ('isPlayed','-karma',)


class Voter(models.Model):
    playlist = models.ForeignKey(Playlist, blank=False)
