from api import views
from django.conf.urls import patterns, url, include
from rest_framework.routers import DefaultRouter
router = DefaultRouter()
#router.register(r'api', views.SnippetViewSet)
#router.register(r'users', views.UserViewSet)
urlpatterns = patterns('',
    url(r'^playlists/$', views.PlaylistList.as_view()),
    url(r'^playlists/(?P<pk>[0-9]+)/$', views.PlaylistDetail.as_view()),
    url(r'^songs/$', views.SongList.as_view()),
    url(r'^songs/(?P<pk>[0-9]+)/$', views.SongDetail.as_view()),
    url(r'^vote/(?P<pk>[0-9]+)/(?P<vote>[ud]+)/$', views.Voting.as_view()),
    url(r'^play/(?P<pk>[0-9]+)/$', views.StartSong.as_view()),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)