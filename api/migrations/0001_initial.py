# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Playlist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
            options={
                'ordering': ('id',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Song',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('soundCloudID', models.IntegerField(unique=True)),
                ('songTitle', models.CharField(max_length=200)),
                ('isPlayed', models.BooleanField(default=False)),
                ('isPlaying', models.BooleanField(default=False)),
                ('karma', models.IntegerField(default=0)),
                ('playlist', models.ForeignKey(related_name='songs', to='api.Playlist')),
            ],
            options={
                'ordering': ('isPlayed', '-karma'),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Voter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('playlist', models.ForeignKey(to='api.Playlist')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
