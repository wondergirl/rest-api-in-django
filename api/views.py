from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics
from api.models import Playlist, Song
from api.serializers import *
from django.http import Http404
from rest_framework import mixins
from rest_framework import generics
from rest_framework.throttling import AnonRateThrottle


class PlaylistList(gedenerics.ListCreateAPIView):
    queryset = Playlist.objects.all()
    serializer_class = PlaylistSerializer
    paginate_by = None


class PlaylistDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Playlist.objects.all()
    serializer_class = PlaylistSerializerDetailed
    paginate_by = None


class SongList(generics.ListCreateAPIView):
    queryset = Song.objects.all()
    serializer_class = SongSerializer
    paginate_by = None

class SongDetail(generics.ListAPIView):
    queryset = Song.objects.all()
    serializer_class = SongSerializer
    paginate_by = None


class StartSong(APIView):

    def get(self, request, pk, format=None):
        if Song.objects.get(isPlaying=True).exists():
            Song.objects.get(isPlaying=True).isPlaying = False
        if Song.objects.get(isPlayed=False).isEmpty:
            pass



        playingSong = Song.objects.get(pk=pk)
        playingSong.isPlayed = True

    pass

class Voting(APIView):

    class VotingLimit(AnonRateThrottle):
        rate = '1/minute'

    throttle_classes = (VotingLimit,)

    def get(self, request, pk, vote, format=None):
        queryset = Song.objects.get(pk=pk)
        if vote == 'u':
           queryset.karma += 1
        elif vote == 'd':
           queryset.karma -= 1

        queryset.save()
        serializer = SongSerializer(queryset)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

class PlayerControl(generics.RetrieveAPIView):
    #queryset = blah
    pass