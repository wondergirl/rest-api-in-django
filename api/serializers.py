from api.models import Song, Playlist
from rest_framework import serializers


class SongSerializer(serializers.ModelSerializer):

    class Meta:
        model = Song

class SongVotingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Song

class PlaylistSerializer(serializers.ModelSerializer):


    class Meta:
        model = Playlist


class PlaylistSerializerDetailed(serializers.ModelSerializer):

    songs = SongSerializer(many=True, read_only=True)

    class Meta:
        model = Playlist

    depth = 3
